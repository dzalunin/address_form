<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Форма</title>

	<script src="<?php echo base_url() ?>js/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap-theme.min.css">
</head>
<body>


<script>
var base_url = '<?php echo site_url('') ?>';
$(document).ready(function(){
  $('form.address_form').submit(function(event){
    alert('submit address');
    var city = $('select[name="city"]').val();
    var street = $('input[name="street"]').val();
    var building_no = $('input[name="building_no"]').val();
    var flat_no = $('input[name="flat_no"]').val();
    var req= $.post(base_url + '/address/put', 
    { city: city, street: street, building_no: building_no, flat_no: flat_no })
    req.done(function(addr){
        var template = '<div>Адрес:<p>Город:'+ addr.city+ '</p>' +
                  '<p>Улица:'+addr.street+' </p>' +
                  '<p>Дом:'+addr.building_no+ '</p>' +
                  '<p>Квартира:'+addr.flat_no+'</p></div>';
        $('.address_list').append(template);
        console.log(template);
    });
    req.fail(function(resp){
        console.log(resp);
    });

    event.preventDefault();
  });
});
</script>

<?php echo validation_errors(); ?>

<div id="container">

  <?php echo form_open('address/create', 'class="address_form"'); ?>
		<label class="label-end-pro">Город:</label>
		<select name="city">
			<?php foreach ($cities as $city):?>
			<option value="<?php echo $city ?>"><?php echo $city ?></option>
			<?php endforeach;?>
		</select></br>

		<label> Улица:</label>
		<input type="text" name="street"/></br>
		<label>Дом:</label>
		<input type="text" name="building_no"/></br>
		<label>Квартира:</label>
		<input type="text" name="flat_no"/></br>

		<input class="price-en4" type="submit" value="Отправить"/>

	</form>
</div>
<div class="address_list">
        <?php foreach ($addresses as $address):?>
          <div>Адрес:
                  <p>Город:<?php echo $address['city'] ?> </p>
                  <p>Улица:<?php echo $address['street']?> </p>
                  <p>Дом:<?php echo $address['building_no'] ?> </p>
                  <p>Квартира:<?php echo $address['flat_no'] ?> </p>
          </div>
        <?php endforeach;?>

</div>
</body>
</html>

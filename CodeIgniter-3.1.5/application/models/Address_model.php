<?php

class Address_model extends CI_Model {

  public function __construct() {
    $this->load->database();
  }

  public function get_addresses() {
    $query =  $this->db->get('addresses');
    return $query->result_array();
  }

  public function set_address($address) {
    $this->load->helper('url');
    $data = array('city'=>$address['city'], 'building_no'=>$address['building_no'], 'flat_no'=> $address['flat_no'],
                  'street'=> $address['street']);

    return $this->db->insert('addresses', $data);
  }
}

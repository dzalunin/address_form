<?php 

class Address extends CI_Controller {
  
  private $addresses;

  function __construct() {
    parent::__construct();
    $this->load->model('address_model');
  }

  public function add() {
    $this->load->helper('url');
    $this->load->helper('form');
    $this->load->library('form_validation'); 
    
    $data['cities'] = array("Минск", "Гродно", "Могилев", "Витебск", "Гомель", "Брест");
    $data['addresses'] = $this->address_model->get_addresses();
    $this->load->view('address', $data);
  }

  public function get(){
    $addresses = $this->address_model->get_addresses();
    
    $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($addresses));
  }

  public function create() {
    $this->load->helper('form');
    $this->load->library('form_validation'); 
    $this->load->helper('url');

    $this->form_validation->set_rules('city', 'Город', 'required');
    $this->form_validation->set_rules('street', 'Улица', 'required');
    $this->form_validation->set_rules('building_no', 'Дом', 'numeric');
    $this->form_validation->set_rules('flat_no', 'Квартира', 'numeric');

    if ($this->form_validation->run() === TRUE)
    {
        $address['city']=$this->input->post('city');
        $address['street']=$this->input->post('street');
        $address['building_no']=$this->input->post('building_no');
        $address['flat_no']=$this->input->post('flat_no');
        $this->address_model->set_address($address);
    }
  
    $this->add(); 
  }

  public function put() {
    $this->load->helper('url');
    $this->load->library('form_validation'); 
    
    $address = array("city"=>$this->input->post('city'),
                     "street"=>$this->input->post('street'),
                     "building_no"=>$this->input->post('building_no'),
                     'flat_no'=>$this->input->post('flat_no'));
    
    $this->form_validation->set_data($address);
    $this->form_validation->set_rules('city', 'Город', 'required');
    $this->form_validation->set_rules('street', 'Улица', 'required');
    $this->form_validation->set_rules('building_no', 'Дом', 'numeric');
    $this->form_validation->set_rules('flat_no', 'Квартира', 'numeric');

    if ($this->form_validation->run() === TRUE) {
        $this->address_model->set_address($address);
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($address));
    }else {
      $this->output
          ->set_status_header(500)
          ->set_content_type('application/json')
          ->set_output(json_encode(array('status' => 'error' )));
    }
  }
}

